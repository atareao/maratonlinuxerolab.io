---
layout: page
title: Maratón Linuxero en vivo
---
Aquí tienes el vídeo en bruto, los 9 directos:

{% include youtubePlayer.html id="Yv90j2HVg1Q" %}

Hemos decidido utilizar YouTube para emitir en directo por su facilidad, el poder llegar a muchos linuxeros que ya tienen cuenta y por el chat en vivo que facilita mucho el feedback:
<{{ site.youtube }}/live>

Si esta opción no es de tu agrado, puedes utilizar nuestra radio oficial que emite en formato OGG Vorbis mediante Icecast: [Radio Maratón](http://radiomaraton.ml)

{% include audioPlayer.html audio="http://200.24.229.253:8000/maratonlinuxero" %}

Si utilizas dispositivos iOS, utiliza el siguiente reproductor con formato MP3:

{% include audioPlayer.html audio="http://200.24.229.253:8888/;?type=http" %}

Dan Bernal Tapia ha creado una app de Radio Maratón para Android: [Descarga](/maraton-linuxero_1.3.apk)

Para no perderte ninguna de las pruebas y el directo del 3 de septiembre, lo mejor es suscribirte a nuestro canal: <{{ site.youtube }}>

Aquí tienes la lista de reproducción de los previos:

{% include youtubePlayer.html id="videoseries?list=PLz7ZCufmrnKJCLvFetPvz2mdiBy4vSmKf" %}
