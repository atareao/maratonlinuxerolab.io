---
layout: page
title: Horario del Maratón Linuxero
---
![#CartelDirectos](/media/carteldirectosmaratonlinuxero.png)  
###### Obra realizada por Dan Bernal Tapia (CC BY-NC-SA)

Si quieres más información, no dudes en ponerte en [contacto](/contacto) y colaborar si así lo deseas.
